﻿using System;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure.Repositories;
using GymMembers.Infrastructure.Repositories.Interfaces;
using GymMembers.Infrastructure.Repositories.Mock;

namespace GymMembers.Infrastructure
{
    public class MockUnitOfWork : IUnitOfWork
    {
        private ICustomerRepository customerRepository;
        private IRepository<Discount> discountRepository;
        private IRepository<DiscountType> discountTypeRepository;
        private IRepository<Membership> membershipRepository;
        private IRepository<MembershipType> membershipTypeRepository;
        private IRepository<PostalAddress> postalAddressRepository;
        private IRepository<Session> sessionRepository;
        private IRepository<Gender> genderRepository;

        public void SaveChanges()
        {
        }

        private bool disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {
                return customerRepository ?? (customerRepository = new MockCustomerRepository());
            }
        }

        public IRepository<Discount> DiscountRepository
        {
            get
            {
                return discountRepository ?? (discountRepository = new MockDiscountRepository());
            }
        }

        public IRepository<DiscountType> DiscountTypeRepository
        {
            get
            {
                return discountTypeRepository ?? (discountTypeRepository = new MockDiscountTypeRepository());
            }
        }

        public IRepository<Membership> MembershipRepository
        {
            get
            {
                return membershipRepository ?? (membershipRepository =  new MockMembershipRepository());
            }
        }

        public IRepository<MembershipType> MembershipTypeRepository
        {
            get
            {
                return membershipTypeRepository ?? (membershipTypeRepository = new MockMembershipTypeRepository());
            }
        }

        public IRepository<PostalAddress> PostalAddressRepository
        {
            get
            {
                return postalAddressRepository ?? (postalAddressRepository = new MockPostalAddressRepository());
            }
        }

        public IRepository<Session> SessionRepository
        {
            get
            {
                return sessionRepository ?? (sessionRepository = new MockSessionRepository());
            }
        }

        public IRepository<Gender> GenderRepository
        {
            get
            {
                return genderRepository ?? (genderRepository = new MockGenderRepository());
            }
        }
    }
}