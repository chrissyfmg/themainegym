﻿using System.Data.Entity;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class DiscountRepository : EntityFrameworkRepository<Discount>
    {
        public DiscountRepository(DbContext context) : base(context)
        {
        }
    }
}