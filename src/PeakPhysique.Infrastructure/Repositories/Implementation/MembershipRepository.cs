﻿using System.Data.Entity;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class MembershipRepository : EntityFrameworkRepository<Membership>
    {
        public MembershipRepository(DbContext context) : base(context)
        {
        }
    }
}