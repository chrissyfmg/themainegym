﻿using System.Data.Entity;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class GenderRepository: EntityFrameworkRepository<Gender>
    {
        public GenderRepository(DbContext context) : base(context)
        {
        }
    }
}
