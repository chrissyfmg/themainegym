﻿using System.Data.Entity;
using System.Linq;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure.Repositories.Interfaces;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class CustomerRepository : EntityFrameworkRepository<Customer>, ICustomerRepository
    {
        private readonly DbContext context;

        public CustomerRepository(DbContext context) : base (context)
        {
            this.context = context;
        }

        public int HighestCustomerNumber
        {
            get
            {
                return context.Set<Customer>().Any() ? context.Set<Customer>().Max(x => x.Number) : 0;
            }
        }
    }
}