﻿using System.Data.Entity;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class PostalAddressRepository : EntityFrameworkRepository<PostalAddress>
    {
        public PostalAddressRepository(DbContext context) : base(context)
        {
        }
    }
}