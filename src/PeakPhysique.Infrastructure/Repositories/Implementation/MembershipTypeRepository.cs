﻿using System.Data.Entity;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class MembershipTypeRepository : EntityFrameworkRepository<MembershipType>
    {
        public MembershipTypeRepository(DbContext context) : base(context)
        {
        }
    }
}