﻿using System.Data.Entity;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class SessionRepository : EntityFrameworkRepository<Session>
    {
        public SessionRepository(DbContext context) : base(context)
        {
        }
    }
}