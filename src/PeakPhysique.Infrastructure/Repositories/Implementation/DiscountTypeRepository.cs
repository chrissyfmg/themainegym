﻿using System.Data.Entity;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Implementation
{
    public class DiscountTypeRepository : EntityFrameworkRepository<DiscountType>
    {
        public DiscountTypeRepository(DbContext context) : base(context)
        {
        }
    }
}