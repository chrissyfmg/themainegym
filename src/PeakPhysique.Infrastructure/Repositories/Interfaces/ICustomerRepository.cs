﻿using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        int HighestCustomerNumber { get; }
    }
}
