﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace GymMembers.Infrastructure.Repositories
{
    public class EntityFrameworkRepository<T> : IRepository<T> where T : class
    {
        private readonly DbContext context;

        public EntityFrameworkRepository(DbContext context)
        {
            this.context = context;
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return context.Set<T>().Where(predicate);
        }

        public IEnumerable<T> GetAll()
        {
            return context.Set<T>();
        }

        public IQueryable<T> Queryable()
        {
            return context.Set<T>();
        }

        public T GetSingle(Func<T, bool> predicate)
        {
            return context.Set<T>().SingleOrDefault(predicate);
        }

        public void Add(T entity)
        {
            context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            context.Set<T>().Remove(entity);
        }

        public void Delete(Func<T, bool> predicate)
        {
            var entitiesToRemove = context.Set<T>().Where(predicate);

            foreach (var entity in entitiesToRemove)
            {
                context.Set<T>().Remove(entity); 
            }
        }
    }
}