﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GymMembers.Infrastructure.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Get(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();
        IQueryable<T> Queryable();
        T GetSingle(Func<T, bool> predicate);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Func<T, bool> predicate); 
    }
}