﻿using System;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockMembershipRepository : MockRepository<Membership>
    {
        public MockMembershipRepository()
        {
            var membershipTypes = new MockMembershipTypeRepository().GetAll();

            var nextId = 1;
            foreach (var membershipType in membershipTypes)
            {
                entities.Add(new Membership
                {
                    Id = nextId,
                    StartDate = DateTime.UtcNow.ToLocalTime(),
                    EndDate = DateTime.UtcNow.ToLocalTime().AddDays(30),
                    MembershipType = membershipType,
                    PaymentReceived = nextId % 2 == 0,
                    PurchasePrice = nextId * 3.5m
                });

                nextId++;
            }
        }
    }
}