﻿using System;
using System.Collections.Generic;
using System.Linq;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockMembershipTypeRepository : MockRepository<MembershipType>
    {
        public MockMembershipTypeRepository()
        {
            entities = new List<MembershipType>
            {
                new MembershipType{ Id = 1, Duration = MembershipDuration.Daily, Name = "Daily", Price = new decimal(3.50)},
                new MembershipType{ Id = 2, Duration = MembershipDuration.Weekly, Name = "Weekly", Price = new decimal(9.00)},
                new MembershipType{ Id = 3, Duration = MembershipDuration.Monthly, Name = "Monthly", Price = new decimal(25.00)},
                new MembershipType{ Id = 3, Duration = MembershipDuration.Annually, Name = "Anually", Price = new decimal(250.00)}
            };
        }
    }
}