﻿using System;
using System.Collections.Generic;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockSessionRepository : MockRepository<Session>
    {
        public MockSessionRepository()
        {
            entities = new List<Session>
            {
                new Session{Id = 1, StartTime = DateTime.UtcNow.ToLocalTime(), EndTime = DateTime.UtcNow.ToLocalTime().AddHours(1)},
                new Session{Id = 2, StartTime = DateTime.UtcNow.ToLocalTime(), EndTime = DateTime.UtcNow.ToLocalTime().AddHours(2)},
                new Session{Id = 3, StartTime = DateTime.UtcNow.ToLocalTime(), EndTime = DateTime.UtcNow.ToLocalTime().AddHours(3)},
                new Session{Id = 4, StartTime = DateTime.UtcNow.ToLocalTime(), EndTime = DateTime.UtcNow.ToLocalTime().AddHours(4)}
            };
        }
    }
}