﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure.Repositories.Interfaces;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockCustomerRepository : MockRepository<Customer>, ICustomerRepository
    {
        public MockCustomerRepository()
        {
            var postalAddressRepo = new MockPostalAddressRepository();
            var sessionRepo = new MockSessionRepository();
            var discountRepo = new MockDiscountRepository();
            var membershipRepo = new MockMembershipRepository();
            var genderRepo = new MockGenderRepository();

            entities = new List<Customer>
            {
                
                new Customer
                {
                    Id = 1, 
                    ContactNumber = "0987654321",
                    DateOfBirth = DateTime.UtcNow.ToLocalTime().AddYears(-24),
                    FirstName = "Jane",
                    LastName = "Doe",
                    Gender = genderRepo.GetSingle(x => x.Name == "Female"),
                    ImagePath = null,
                    Number = 1002,
                    PostalAddress = postalAddressRepo.GetSingle(x => x.Id == 2),
                    Sessions = sessionRepo.GetAll().ToList(),
                    Discount = discountRepo.GetSingle(x => x.Id == 1),
                    Memberships = membershipRepo.GetAll().ToList()
                },
                new Customer
                {
                    Id = 2, 
                    ContactNumber = "0123456798",
                    DateOfBirth = DateTime.UtcNow.ToLocalTime().AddYears(-21),
                    FirstName = "Joe",
                    LastName = "Bloggs",
                    Gender =  genderRepo.GetSingle(x => x.Name == "Male"),
                    ImagePath = null,
                    Number = 1001,
                    PostalAddress = postalAddressRepo.GetSingle(x => x.Id == 1),
                    Sessions = new List<Session>(),
                    Discount = null,
                    Memberships = new List<Membership>()
                }
            };
        }

        public int HighestCustomerNumber {
            get
            {
                return entities.Any() ? entities.Max(x => x.Number) : 0;

            } 
        }
    }
}