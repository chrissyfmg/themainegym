﻿using System.Collections.Generic;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockPostalAddressRepository : MockRepository<PostalAddress>
    {
        public MockPostalAddressRepository()
        {
            entities = new List<PostalAddress>
            {
                new PostalAddress{ Id = 1, FirstLine = "MockAdd1 Line 1", SecondLine = "MockAdd1 Line 2", Postcode = "Postcode1", Town = "MockAdd1 Town"},
                new PostalAddress{ Id = 2, FirstLine = "MockAdd2 Line 1", SecondLine = "MockAdd2 Line 2", Postcode = "Postcode2", Town = "MockAdd2 Town"},
                new PostalAddress{ Id = 3, FirstLine = "MockAdd3 Line 1", SecondLine = "MockAdd3 Line 2", Postcode = "Postcode3", Town = "MockAdd3 Town"},
                new PostalAddress{ Id = 4, FirstLine = "MockAdd4 Line 1", SecondLine = "MockAdd4 Line 2", Postcode = "Postcode4", Town = "MockAdd4 Town"}
            };
        }
    }
}