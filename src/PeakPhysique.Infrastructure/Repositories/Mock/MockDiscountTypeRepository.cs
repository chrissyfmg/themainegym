﻿using System.Collections.Generic;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockDiscountTypeRepository : MockRepository<DiscountType>
    {
        public MockDiscountTypeRepository()
        {
            entities = new List<DiscountType>
            {
                new DiscountType{ Id = 1, Name = "Student", ReductionPercentage = 5},
                new DiscountType{ Id = 2, Name = "Half Price", ReductionPercentage = 50}
            };
        }
    }
}