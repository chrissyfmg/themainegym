﻿using System.Collections.Generic;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockGenderRepository: MockRepository<Gender>
    {
        public MockGenderRepository()
        {
            entities = new List<Gender>
            {
                new Gender{ Id = 1, Name = "Male"},
                new Gender{ Id = 2, Name = "Female"}
            };
        }
    }
}
