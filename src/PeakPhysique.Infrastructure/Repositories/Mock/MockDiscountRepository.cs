﻿using System;
using System.Collections.Generic;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Repositories.Mock
{
    public class MockDiscountRepository : MockRepository<Discount>
    {
        public MockDiscountRepository()
        {
            var discountTypes = new MockDiscountTypeRepository().GetAll();

            var nextId = 1;
            foreach (var discountType in discountTypes)
            {
                entities.Add(new Discount
                {
                    Id = nextId,
                    DiscountType = discountType,
                    ExpiryDate = DateTime.UtcNow.ToLocalTime().AddMonths(nextId)
                });

                nextId ++;
            }
        }
    }
}