﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GymMembers.Domain;

namespace GymMembers.Infrastructure.Repositories
{
    public abstract class MockRepository<T> : IRepository<T> where T : Entity
    {
        protected IList<T> entities = new List<T>();

        public IQueryable<T> Get(Expression<Func<T, bool>> predicate)
        {
            var query = predicate.Compile();
            return entities.Where(query).AsQueryable();
        }

        public IEnumerable<T> GetAll()
        {
            return entities;
        }

        public IQueryable<T> Queryable()
        {
            return entities.ToList().AsQueryable();
        }

        public T GetSingle(Func<T, bool> predicate)
        {
            return entities.SingleOrDefault(predicate);
        }

        public void Add(T entity)
        {
            entities.Add(entity);
        }

        public void Update(T entity)
        {
            var entityToRemove = GetSingle(x => x.Id == entity.Id);
            entities.Remove(entityToRemove);
            entities.Add(entity);
        }

        public void Delete(T entity)
        {
            entities.Remove(entity);
        }

        public void Delete(Func<T, bool> predicate)
        {
            var entitiesToRemove = entities.Where(predicate);

            foreach (var entity in entitiesToRemove)
            {
                entities.Remove(entity);   
            }
        }
    }
}
