﻿using System;
using System.Data.Entity;
using System.Linq;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure.Persistence;
using GymMembers.Infrastructure.Repositories;
using GymMembers.Infrastructure.Repositories.Implementation;
using GymMembers.Infrastructure.Repositories.Interfaces;

namespace GymMembers.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GymMembersEntities entities;

        private ICustomerRepository customerRepository;
        private IRepository<Discount> discountRepository;
        private IRepository<DiscountType> discountTypeRepository;
        private IRepository<Membership> membershipRepository;
        private IRepository<MembershipType> membershipTypeRepository;
        private IRepository<PostalAddress> postalAddressRepository;
        private IRepository<Session> sessionRepository;
        private IRepository<Gender> genderRepository;

        public UnitOfWork()
        {
            entities = new GymMembersEntities();
        }

        public void SaveChanges()
        {
            entities.SaveChanges();
        }

        private bool disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    entities.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {
                return customerRepository ?? (customerRepository = new CustomerRepository(entities));
            }
        }

        public IRepository<Discount> DiscountRepository
        {
            get
            {
                return discountRepository ?? (discountRepository = new EntityFrameworkRepository<Discount>(entities));
            }
        }

        public IRepository<DiscountType> DiscountTypeRepository
        {
            get
            {
                return discountTypeRepository ?? (discountTypeRepository = new EntityFrameworkRepository<DiscountType>(entities));
            }
        }

        public IRepository<Membership> MembershipRepository
        {
            get
            {
                return membershipRepository ?? (membershipRepository =  new EntityFrameworkRepository<Membership>(entities));
            }
        }

        public IRepository<MembershipType> MembershipTypeRepository
        {
            get
            {
                return membershipTypeRepository ?? (membershipTypeRepository = new EntityFrameworkRepository<MembershipType>(entities));
            }
        }

        public IRepository<PostalAddress> PostalAddressRepository
        {
            get
            {
                return postalAddressRepository ?? (postalAddressRepository = new EntityFrameworkRepository<PostalAddress>(entities));
            }
        }

        public IRepository<Session> SessionRepository
        {
            get
            {
                return sessionRepository ?? (sessionRepository = new EntityFrameworkRepository<Session>(entities));
            }
        }

        public IRepository<Gender> GenderRepository
        {
            get
            {
                return genderRepository ?? (genderRepository = new EntityFrameworkRepository<Gender>(entities));
            }
        }
    }
}