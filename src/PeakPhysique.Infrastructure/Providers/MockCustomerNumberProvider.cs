﻿using GymMembers.Domain;

namespace GymMembers.Infrastructure.Providers
{
    public class MockCustomerNumberProvider : ICustomerNumberProvider
    {
        private int currentNumber = 1000;

        public int GetCustomerNumber()
        {
            return currentNumber++;
        }
    }
}
