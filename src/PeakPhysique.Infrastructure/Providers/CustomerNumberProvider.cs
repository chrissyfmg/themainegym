﻿using System;
using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using GymMembers.Domain;

namespace GymMembers.Infrastructure.Providers
{
    public class CustomerNumberProvider : ICustomerNumberProvider
    {
        public int GetCustomerNumber()
        {
            using (var unitOfWork = ServiceLocator.Current.GetInstance<IUnitOfWork>())
            {
                var highestCustomerNumber = unitOfWork.CustomerRepository.HighestCustomerNumber;

                if (highestCustomerNumber == 0)
                {
                    return Convert.ToInt32(ConfigurationManager.AppSettings["CustomerNumberDefault"]);
                }

                return highestCustomerNumber + 1;
            }
        }
    }
}
