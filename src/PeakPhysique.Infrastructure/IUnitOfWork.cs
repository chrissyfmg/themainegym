﻿using System;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure.Repositories;
using GymMembers.Infrastructure.Repositories.Interfaces;

namespace GymMembers.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomerRepository CustomerRepository { get; }
        IRepository<Discount> DiscountRepository { get; }
        IRepository<DiscountType> DiscountTypeRepository { get; }
        IRepository<Membership> MembershipRepository { get; }
        IRepository<MembershipType> MembershipTypeRepository { get; }
        IRepository<PostalAddress> PostalAddressRepository { get; }
        IRepository<Session> SessionRepository { get; }
        IRepository<Gender> GenderRepository { get; } 
        void SaveChanges();
    }
}