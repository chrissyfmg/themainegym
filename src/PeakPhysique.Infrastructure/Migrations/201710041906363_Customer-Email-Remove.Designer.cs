// <auto-generated />
namespace GymMembers.Infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class CustomerEmailRemove : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CustomerEmailRemove));
        
        string IMigrationMetadata.Id
        {
            get { return "201710041906363_Customer-Email-Remove"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
