namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MembershipTypeActiveFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MembershipTypes", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MembershipTypes", "Active");
        }
    }
}
