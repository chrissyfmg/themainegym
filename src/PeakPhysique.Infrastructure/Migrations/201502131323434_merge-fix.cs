namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mergefix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Memberships", "Deleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Memberships", "Deleted");
        }
    }
}
