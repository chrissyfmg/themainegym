namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerEmailRemove : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Customers", "EmailAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "EmailAddress", c => c.String(maxLength: 4000));
        }
    }
}
