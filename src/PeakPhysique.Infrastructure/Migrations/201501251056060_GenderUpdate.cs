namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GenderUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Customers", "Gender_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Customers", "Gender_Id");
            AddForeignKey("dbo.Customers", "Gender_Id", "dbo.Genders", "Id", cascadeDelete: true);
            DropColumn("dbo.Customers", "Gender");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Gender", c => c.String(nullable: false, maxLength: 4000));
            DropForeignKey("dbo.Customers", "Gender_Id", "dbo.Genders");
            DropIndex("dbo.Customers", new[] { "Gender_Id" });
            DropColumn("dbo.Customers", "Gender_Id");
            DropTable("dbo.Genders");
        }
    }
}
