namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImagePathNotRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "ImagePath", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "ImagePath", c => c.String(nullable: false, maxLength: 4000));
        }
    }
}
