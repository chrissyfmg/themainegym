using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Persistence.GymMembersEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Persistence.GymMembersEntities context)
        {
            context.Gender.AddOrUpdate(x => x.Name,
                    new Gender { Name = "Male"},
                    new Gender { Name = "Female"}
                );

            context.DiscountType.AddOrUpdate(x => x.Name,
                new DiscountType { Name = "Student", ReductionPercentage = 20 });


            context.MembershipType.AddOrUpdate(x => x.Name,
                new MembershipType { Name = "Daily", Duration = MembershipDuration.Daily, Price = 3.50m },
                new MembershipType { Name = "Weekly", Duration = MembershipDuration.Weekly, Price = 9.00m },
                new MembershipType { Name = "Monthly", Duration = MembershipDuration.Monthly, Price = 25.00m },
                new MembershipType { Name = "Annualy", Duration = MembershipDuration.Annually, Price = 240.00m });
        }
    }
}
