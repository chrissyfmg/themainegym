namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableSessionEndDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sessions", "EndTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sessions", "EndTime", c => c.DateTime(nullable: false));
        }
    }
}
