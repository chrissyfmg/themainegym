namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedColumnDiscountType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DiscountTypes", "Deleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DiscountTypes", "Deleted");
        }
    }
}
