namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Memberships", "PaymentDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Memberships", "PaymentDate");
        }
    }
}
