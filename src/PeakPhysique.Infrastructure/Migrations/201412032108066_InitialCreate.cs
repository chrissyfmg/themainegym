namespace GymMembers.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        ImagePath = c.String(nullable: false, maxLength: 4000),
                        FirstName = c.String(nullable: false, maxLength: 4000),
                        LastName = c.String(nullable: false, maxLength: 4000),
                        DateOfBirth = c.DateTime(nullable: false),
                        ContactNumber = c.String(nullable: false, maxLength: 4000),
                        Gender = c.String(nullable: false, maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PostalAddresses", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        DiscountTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DiscountTypes", t => t.DiscountTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.DiscountTypeId);
            
            CreateTable(
                "dbo.DiscountTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        ReductionPercentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Memberships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentReceived = c.Boolean(nullable: false),
                        PurchasePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        MembershipTypeId = c.Int(nullable: false),
                        Customer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.Customer_Id)
                .ForeignKey("dbo.MembershipTypes", t => t.MembershipTypeId, cascadeDelete: true)
                .Index(t => t.MembershipTypeId)
                .Index(t => t.Customer_Id);
            
            CreateTable(
                "dbo.MembershipTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Duration = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PostalAddresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstLine = c.String(nullable: false, maxLength: 100),
                        SecondLine = c.String(maxLength: 100),
                        Town = c.String(nullable: false, maxLength: 50),
                        Postcode = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        Customer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.Customer_Id)
                .Index(t => t.Customer_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sessions", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.Customers", "Id", "dbo.PostalAddresses");
            DropForeignKey("dbo.Memberships", "MembershipTypeId", "dbo.MembershipTypes");
            DropForeignKey("dbo.Memberships", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.Discounts", "Id", "dbo.Customers");
            DropForeignKey("dbo.Discounts", "DiscountTypeId", "dbo.DiscountTypes");
            DropIndex("dbo.Sessions", new[] { "Customer_Id" });
            DropIndex("dbo.Memberships", new[] { "Customer_Id" });
            DropIndex("dbo.Memberships", new[] { "MembershipTypeId" });
            DropIndex("dbo.Discounts", new[] { "DiscountTypeId" });
            DropIndex("dbo.Discounts", new[] { "Id" });
            DropIndex("dbo.Customers", new[] { "Id" });
            DropTable("dbo.Sessions");
            DropTable("dbo.PostalAddresses");
            DropTable("dbo.MembershipTypes");
            DropTable("dbo.Memberships");
            DropTable("dbo.DiscountTypes");
            DropTable("dbo.Discounts");
            DropTable("dbo.Customers");
        }
    }
}
