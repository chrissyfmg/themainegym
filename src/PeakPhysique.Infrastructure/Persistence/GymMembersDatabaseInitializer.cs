﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence
{
    public class GymMembersDatabaseInitializer : CreateDatabaseIfNotExists<GymMembersEntities>
    {
        protected override void Seed(GymMembersEntities context)
        {
            context.Gender.AddOrUpdate(x => x.Name,
                    new Gender { Name = "Male" },
                    new Gender { Name = "Female" }
                );

            //context.DiscountType.AddOrUpdate(x => x.Name,
            //    new DiscountType { Name = "Student", ReductionPercentage = 20 });


            context.MembershipType.AddOrUpdate(x => x.Name,
                new MembershipType { Name = "Daily", Duration = MembershipDuration.Daily, Price = 4.00m, Active = true },
                new MembershipType { Name = "Weekly", Duration = MembershipDuration.Weekly, Price = 9.00m, Active = false },
                new MembershipType { Name = "Monthly", Duration = MembershipDuration.Monthly, Price = 20.00m, Active = true },
                new MembershipType { Name = "Annual", Duration = MembershipDuration.Annually, Price = 240.00m, Active = false });

            base.Seed(context);
        }
    }
}