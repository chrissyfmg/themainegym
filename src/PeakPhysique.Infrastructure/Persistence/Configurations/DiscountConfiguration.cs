﻿using System.Data.Entity.ModelConfiguration;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence.Configurations
{
    public class DiscountConfiguration : EntityTypeConfiguration<Discount>
    {
        public DiscountConfiguration()
        {
            Property(x => x.ExpiryDate).IsRequired();
        }
    }
}