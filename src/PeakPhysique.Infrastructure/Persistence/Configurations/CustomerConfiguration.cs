﻿using System.Data.Entity.ModelConfiguration;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence.Configurations
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            Property(x => x.Number).IsRequired();
            Property(x => x.ImagePath);
            Property(x => x.FirstName).IsRequired();
            Property(x => x.LastName).IsRequired();
            Property(x => x.DateOfBirth).IsRequired();
            Property(x => x.ContactNumber).IsRequired();

            HasRequired(x => x.Gender);
            HasOptional(x => x.Discount).WithRequired(x => x.Customer);
            HasRequired(x => x.PostalAddress).WithRequiredDependent(x => x.Customer);
        } 
    }
}