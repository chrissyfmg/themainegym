﻿using System.Data.Entity.ModelConfiguration;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence.Configurations
{
    public class MembershipConfiguration : EntityTypeConfiguration<Membership>
    {
        public MembershipConfiguration()
        {
            Property(x => x.PaymentDate).IsOptional();
            Property(x => x.PaymentReceived).IsRequired();
            Property(x => x.PurchasePrice).IsRequired();
            Property(x => x.StartDate).IsRequired();
            Property(x => x.EndDate).IsRequired();
        }
    }
}