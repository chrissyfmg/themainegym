﻿using System.Data.Entity.ModelConfiguration;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence.Configurations
{
    public class DiscountTypeConfiguration : EntityTypeConfiguration<DiscountType>
    {
        public DiscountTypeConfiguration()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.ReductionPercentage);

            HasMany(x => x.Discounts).WithRequired(x => x.DiscountType).WillCascadeOnDelete();
        }
    }
}
