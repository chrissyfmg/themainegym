﻿using System.Data.Entity.ModelConfiguration;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence.Configurations
{
    public class PostalAddressConfiguration : EntityTypeConfiguration<PostalAddress>
    {
        public PostalAddressConfiguration()
        {
            Property(x => x.FirstLine).IsRequired().HasMaxLength(100);
            Property(x => x.SecondLine).HasMaxLength(100);
            Property(x => x.Town).IsRequired().HasMaxLength(50);
            Property(x => x.Postcode).IsRequired().HasMaxLength(10);
        }
    }
}
