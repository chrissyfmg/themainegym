﻿using System.Data.Entity.ModelConfiguration;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence.Configurations
{
    public class MembershipTypeConfiguration : EntityTypeConfiguration<MembershipType>
    {
        public MembershipTypeConfiguration()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.Duration).IsRequired();
        }
    }
}