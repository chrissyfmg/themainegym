﻿using System.Data.Entity;
using System.Reflection;
using GymMembers.Domain.Models;

namespace GymMembers.Infrastructure.Persistence
{
    public class GymMembersEntities : DbContext
    {
        public GymMembersEntities() : base("GymMembersEntities")
        {
            Database.SetInitializer(new GymMembersDatabaseInitializer());
        }

        public DbSet<Customer> Customer { get; set; }
        public DbSet<Discount> Discount { get; set; }
        public DbSet<DiscountType> DiscountType { get; set; }
        public DbSet<Membership> Membership { get; set; }
        public DbSet<MembershipType> MembershipType { get; set; }
        public DbSet<PostalAddress> PostalAddress { get; set; }
        public DbSet<Session> Session { get; set; }
        public DbSet<Gender> Gender { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
