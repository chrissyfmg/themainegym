﻿namespace GymMembers.Utilities.Exporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Microsoft.Win32;
    using ClosedXML.Excel;

    public class ExcelExport
    {
        public bool DownloadReportXls<T>(List<T> data)
        {
            var properties = TypeDescriptor.GetProperties(typeof(T));

            var workbook = new XLWorkbook();
            workbook.AddWorksheet("sheet1");
            var ws = workbook.Worksheet("sheet1");

            int cell = 1;
            int row = 2;

            foreach (PropertyDescriptor prop in properties)
            {
                ws.Cell(1, cell).Value = prop.Name;
                cell++;
            }

            cell = 1;

            foreach (T item in data)
            {
                foreach (PropertyDescriptor prop in properties)
                {
                    ws.Cell(row, cell).Value = prop.GetValue(item) ?? string.Empty;
                    cell++;
                }

                row++;
                cell = 1;
            }

            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Excel files|*.xlsx",
                Title = "Download Report",
                DefaultExt = ".xlsx",
                FileName = string.Format("Report_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd_hhmmss"))
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                if (!String.IsNullOrWhiteSpace(saveFileDialog.FileName))
                {
                    workbook.SaveAs(saveFileDialog.FileName);
                    return true;
                }
            }

            return false;
        }
    }
}