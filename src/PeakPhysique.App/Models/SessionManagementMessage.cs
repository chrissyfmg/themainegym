﻿namespace GymMembers.App.Models
{
    public class SessionManagementMessage
    {
        public bool SignedOut { get; set; }
        public int CustomerNumber { get; set; }
    }
}