﻿namespace GymMembers.App.Models
{
    public enum CameraState
    {
        Preview,
        Taken,
        Error
    }
}