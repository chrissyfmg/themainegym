﻿namespace GymMembers.App.Models
{
    public enum NfcState
    {
        Disconnected,
        Ready, 
        Register,
        Erase
    }
}