﻿using System;
using System.IO;
using System.Windows.Controls;

namespace GymMembers.App.Models
{
    internal class NavigationMessage
    {
        public NavigationMessage(int targetEntityId, Type pageType)
        {
            if (pageType.BaseType != typeof (Page))
            {
                throw new InvalidOperationException("You must pass the type of a valid page");
            }

            PageName = pageType.Name;
        }

        public string PageName { get; private set; }
    }
}
