﻿namespace GymMembers.App.Models
{
    public class CameraStateMessage
    {
        public CameraStateMessage(CameraState state, string data = null)
        {
            State = state;
            Data = data;
        }

        public CameraState State { get; set; }
        public string Data { get; set; }
    }
}