﻿using System;
using System.Security.Policy;

namespace GymMembers.App.Models
{
    public class PageLink
    {
        public bool IsInitialPage { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public Uri PageUri { get; set; }
        public Uri IconUri { get; set; }
    }
}
