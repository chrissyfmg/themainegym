﻿using System;

namespace GymMembers.App.Models
{
    public class Notification
    {
        public string Message { get; set; }
        public NotificationType NotificationType { get; set; }
        public Uri NotificationIconUrl { get; set; }
    }

    public enum NotificationType
    {
        Success,
        Error,
        Busy
    }
}
