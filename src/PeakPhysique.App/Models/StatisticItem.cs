﻿namespace GymMembers.App.Models
{
    public class StatisticItem
    {
        public int Count { get; set; }

        public string Title { get; set; }
    }
}
