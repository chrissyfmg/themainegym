﻿namespace GymMembers.App.Models
{
    public class IncomeStatisticItem : StatisticItem
    {
        public decimal Sum { get; set; }
    }
}
