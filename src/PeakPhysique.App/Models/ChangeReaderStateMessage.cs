﻿namespace GymMembers.App.Models
{
    internal class ChangeReaderStateMessage
    {
        public int CustomerNumber { get; set; }
        public NfcState NewState { get; set; }
    }
}
