﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GymMembers.App.Models;
using GymMembers.App.Virtualization;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure;
using GymMembers.Utilities.Exporting;

namespace GymMembers.App.ViewModels
{
    public class DueMembershipsViewModel : ViewModelBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly QueryableItemsProvider<Customer> customerProvider;

        public DueMembershipsViewModel(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            customerProvider = new QueryableItemsProvider<Customer>();

            CreateCommands();
            InitializeData();

            UpdateCustomerList(true);
        }

        private void RefreshCustomerList()
        {
            PaymentDueCustomerCollection = new VirtualizingCollection<Customer>(customerProvider, 10);
        }

        private void UpdateCustomerList(bool selectFirst = false)
        {
            UpdateCustomerQuery();
            RefreshCustomerList();

            if (selectFirst)
            {
                SelectedCustomer = PaymentDueCustomerCollection.FirstOrDefault();
            }

            if(PaymentDueCustomerCollection.Count == 0 && string.IsNullOrWhiteSpace(SearchValue))
            {
                ShowButtons = false;
            }
            else
            {
                ShowButtons = true;
            }
        }

        private void InitializeData()
        {
            MembershipTypes = new ObservableCollection<MembershipType>(unitOfWork.MembershipTypeRepository.GetAll());
        }

        private void CreateCommands()
        {
            ClearSearchCommand = new RelayCommand(ClearSearch);
            UpdateMembershipsCommand = new RelayCommand(UpdateMemberships);
            DownloadReportCommand = new RelayCommand(DownloadReport);
        }

        private void RefrehMemberships()
        {
            CustomerMemberships = SelectedCustomer != null ? new ObservableCollection<Membership>(SelectedCustomer.Memberships) : null;
        }

        private VirtualizingCollection<Customer> paymentDueCustomerCollection;
        public VirtualizingCollection<Customer> PaymentDueCustomerCollection
        {
            get
            {
                return paymentDueCustomerCollection;
            }
            set
            {
                if (paymentDueCustomerCollection != value)
                {
                    paymentDueCustomerCollection = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string searchValue;
        public string SearchValue
        {
            get { return searchValue; }
            set
            {
                searchValue = value;
                SelectedCustomer = null;
                UpdateCustomerList();
                RaisePropertyChanged();
            }
        }

        private bool showButtons;
        public bool ShowButtons
        {
            get { return showButtons; }
            set
            {
                if (showButtons != value)
                {
                    showButtons = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Customer selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                RefrehMemberships();
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<Membership> customerMemberships;
        public ObservableCollection<Membership> CustomerMemberships
        {
            get { return customerMemberships; }
            set
            {
                if (customerMemberships != value)
                {
                    customerMemberships = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<MembershipType> membershipTypes;
        public ObservableCollection<MembershipType> MembershipTypes
        {
            get { return membershipTypes; }
            set
            {
                if (membershipTypes != value)
                {
                    membershipTypes = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand UpdateMembershipsCommand { get; set; }
        public RelayCommand ClearSearchCommand { get; set; }
        public RelayCommand DownloadReportCommand { get; set; }

        private void ClearSearch()
        {
            if (string.IsNullOrEmpty(searchValue))
            {
                return;
            }

            SearchValue = string.Empty;
        }

        private void DownloadReport()
        {
            var query = unitOfWork.MembershipRepository.Get(x => !x.PaymentReceived)
                .Select(x => new
                {
                    Customer = x.Customer.FirstName + " " + x.Customer.LastName,
                    MembershipType = x.MembershipType.Name,
                    AmountOwed = x.PurchasePrice,
                    MembershipStartDate = x.StartDate,
                    EmailAddress = x.Customer.EmailAddress
                }).ToList();

            try
            {
                var excel = new ExcelExport();
                var downloaded = excel.DownloadReportXls(query);

                if (downloaded)
                {
                    Messenger.Default.Send(new Notification
                    {
                        Message = "Report downloaded",
                        NotificationType = NotificationType.Success
                    });
                }
            }
            catch (Exception ex)
            {
                Messenger.Default.Send(new Notification
                {
                    Message = "Report failed",
                    NotificationType = NotificationType.Error
                });
            }
        }

        private void UpdateCustomerQuery()
        {
            var customerQuery = unitOfWork.CustomerRepository.Queryable().Where(x => x.Memberships.Count > 0 && x.Memberships.Any(y => !y.PaymentReceived));

            if (!string.IsNullOrWhiteSpace(SearchValue))
            {
                customerQuery = customerQuery.Where(x => x.FirstName.Contains(SearchValue)
                                                         || x.LastName.Contains(SearchValue)
                                                         || x.Number.ToString().Contains(SearchValue));
            }

            customerProvider.Queryable = customerQuery;
        }

        private void UpdateMemberships()
        {
            try
            {
                unitOfWork.SaveChanges();
                if (SelectedCustomer.Memberships.All(x => x.PaymentReceived))
                {
                    SelectedCustomer = null;
                }

                UpdateCustomerList();

                Messenger.Default.Send(new Notification { Message = "Memberships updated", NotificationType = NotificationType.Success });
            }
            catch (Exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to update memberships", NotificationType = NotificationType.Error });
            }
        }
    }

}
