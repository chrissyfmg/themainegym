﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GymMembers.App.Models;
using GymMembers.Infrastructure;

namespace GymMembers.App.ViewModels
{
    public class StatisticsViewModel : ViewModelBase
    {
        private readonly IUnitOfWork unitOfWork;

        private DateTime previousWeekStartDate;
        private DateTime previousMonthStartDate;
        private DateTime previousYearStartDate;

        public StatisticsViewModel(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            previousWeekStartDate = DateTime.Today.AddDays(-7);
            previousMonthStartDate = DateTime.Today.AddMonths(-1);
            previousYearStartDate = DateTime.Today.AddYears(-1);

            PurchasedMembershipTypes = GetMembershipStats();
            PastWeekMemberships = GetPastWeeksMemberships();
            PastMonthMemberships = GetPastMonthsMemberships();
            PastYearMemberships = GetPastYearMemberships();
            PercentagePaid = GetPercentagePaid();
            Attendance = GetAttendanceStats();
            RecentIncome = GetIncomeStats();
            InitializeIncomeByDateSummary();

            GetIncomeSummaryCommand = new RelayCommand(UpdateIncomeByDateSummary);
        }

        public RelayCommand GetIncomeSummaryCommand { get; set; }

        private string incomeByDateSummary;
        public string IncomeByDateSummary
        {
            get
            {
                return incomeByDateSummary;
            }
            set
            {
                if (incomeByDateSummary != value)
                {
                    incomeByDateSummary = value;
                    RaisePropertyChanged();
                }
            }
        }

        private DateTime incomeByDateStart;
        public DateTime IncomeByDateStart
        {
            get
            {
                return incomeByDateStart;
            }
            set
            {
                if (incomeByDateStart != value)
                {
                    incomeByDateStart = value;
                    RaisePropertyChanged();
                }
            }
        }

        private DateTime incomeByDateEnd;
        public DateTime IncomeByDateEnd
        {
            get
            {
                return incomeByDateEnd;
            }
            set
            {
                if (incomeByDateEnd != value)
                {
                    incomeByDateEnd = value.AddDays(1);
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<StatisticItem> purchasedMembershipTypes;
        public ObservableCollection<StatisticItem> PurchasedMembershipTypes
        {
            get
            {
                return purchasedMembershipTypes;
            }
            set
            {
                if (purchasedMembershipTypes != value)
                {
                    purchasedMembershipTypes = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<StatisticItem> pastWeekMemberships;
        public ObservableCollection<StatisticItem> PastWeekMemberships
        {
            get
            {
                return pastWeekMemberships;
            }
            set
            {
                if (pastWeekMemberships != value)
                {
                    pastWeekMemberships = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<StatisticItem> pastMonthMemberships;
        public ObservableCollection<StatisticItem> PastMonthMemberships
        {
            get
            {
                return pastMonthMemberships;
            }
            set
            {
                if (pastMonthMemberships != value)
                {
                    pastMonthMemberships = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<StatisticItem> pastYearMemberships;
        public ObservableCollection<StatisticItem> PastYearMemberships
        {
            get
            {
                return pastYearMemberships;
            }
            set
            {
                if (pastYearMemberships != value)
                {
                    pastYearMemberships = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<StatisticItem> percentagePaid;
        public List<StatisticItem> PercentagePaid
        {
            get
            {
                return percentagePaid;
            }
            set
            {
                if (percentagePaid != value)
                {
                    percentagePaid = value;
                }
            }
        }

        private ObservableCollection<StatisticItem> attendance;
        public ObservableCollection<StatisticItem> Attendance
        {
            get
            {
                return attendance;
            }
            set
            {
                if (attendance != value)
                {
                    attendance = value;
                }
            }
        }

        private ObservableCollection<IncomeStatisticItem> recentIncome;
        public ObservableCollection<IncomeStatisticItem> RecentIncome
        {
            get
            {
                return recentIncome;
            }
            set
            {
                if (recentIncome != value)
                {
                    recentIncome = value;
                }
            }
        }

        private ObservableCollection<StatisticItem> GetMembershipStats()
        {
            return new ObservableCollection<StatisticItem>(unitOfWork.MembershipRepository.Queryable().GroupBy(l => l.MembershipType.Name)
                    .Select(g => new StatisticItem
                    {
                        Title = g.Key,
                        Count = g.Distinct().Count()
                    }).ToList());
        }

        private ObservableCollection<StatisticItem> GetPastWeeksMemberships()
        {
            var previousWeekMemberships = unitOfWork.MembershipRepository.Get(x => x.StartDate >= previousWeekStartDate).GroupBy(l => l.MembershipType.Name)
                    .Select(g => new StatisticItem
                    {
                        Title = g.Key,
                        Count = g.Distinct().Count()
                    }).ToList();

            return new ObservableCollection<StatisticItem>(previousWeekMemberships);
        }

        private ObservableCollection<StatisticItem> GetPastMonthsMemberships()
        {
            var previousMonthMemberships = unitOfWork.MembershipRepository.Get(x => x.StartDate >= previousMonthStartDate).GroupBy(l => l.MembershipType.Name)
                    .Select(g => new StatisticItem
                    {
                        Title = g.Key,
                        Count = g.Distinct().Count()
                    }).ToList();

            return new ObservableCollection<StatisticItem>(previousMonthMemberships);
        }

        private ObservableCollection<StatisticItem> GetPastYearMemberships()
        {
            var previousYearMemberships = unitOfWork.MembershipRepository.Get(x => x.StartDate >= previousYearStartDate).GroupBy(l => l.MembershipType.Name)
                    .Select(g => new StatisticItem
                    {
                        Title = g.Key,
                        Count = g.Distinct().Count()
                    }).ToList();

            return new ObservableCollection<StatisticItem>(previousYearMemberships);
        }

        public List<StatisticItem> GetPercentagePaid()
        {
            var percentageOfPaymentReceived = 0;

            if (unitOfWork.MembershipRepository.Queryable().Any())
            {
                decimal amountPaid = 0m;
                var totalAmount = unitOfWork.MembershipRepository.Queryable().Sum(x => x.PurchasePrice);

                if (unitOfWork.MembershipRepository.Queryable().Any(x => x.PaymentReceived))
                {
                    amountPaid = unitOfWork.MembershipRepository.Get(x => x.PaymentReceived).Sum(x => x.PurchasePrice);
                }

                percentageOfPaymentReceived = (int)((amountPaid / totalAmount) * 100);
            }

            return new List<StatisticItem>{ new StatisticItem {Count = percentageOfPaymentReceived, Title = "Percentage Paid"}};
        }

        public ObservableCollection<StatisticItem> GetAttendanceStats()
        {
            var dayCount = unitOfWork.SessionRepository.Get(x => x.EndTime >= DateTime.Today).Count();

            var weekCount = unitOfWork.SessionRepository.Get(x => x.EndTime >= previousWeekStartDate).Count();

            var monthCount = unitOfWork.SessionRepository.Get(x => x.EndTime >= previousMonthStartDate).Count();

            var yearCount = unitOfWork.SessionRepository.Get(x => x.EndTime >= previousYearStartDate).Count();

            return new ObservableCollection<StatisticItem>
            {
                new StatisticItem { Count = dayCount, Title = "Today"},
                new StatisticItem { Count = weekCount, Title = "Past Week"},
                new StatisticItem { Count = monthCount, Title = "Past Month"},
                new StatisticItem { Count = yearCount, Title = "Past Year"}
            };
        }

        public ObservableCollection<IncomeStatisticItem> GetIncomeStats()
        {
            var today = DateTime.Today;

            var dayCount = unitOfWork.MembershipRepository.Get(x => x.StartDate >= today && x.PaymentReceived).ToArray().Sum(x => x.PurchasePrice);

            var weekCount = unitOfWork.MembershipRepository.Get(x => x.StartDate >= previousWeekStartDate && x.PaymentReceived).ToArray().Sum(x => x.PurchasePrice);

            var monthCount = unitOfWork.MembershipRepository.Get(x => x.StartDate >= previousMonthStartDate && x.PaymentReceived).ToArray().Sum(x => x.PurchasePrice);

            var yearCount = unitOfWork.MembershipRepository.Get(x => x.StartDate >= previousYearStartDate && x.PaymentReceived).ToArray().Sum(x => x.PurchasePrice);

            return new ObservableCollection<IncomeStatisticItem>
            {
                new IncomeStatisticItem { Sum = dayCount, Title = "Today"},
                new IncomeStatisticItem { Sum = weekCount, Title = "Past Week"},
                new IncomeStatisticItem { Sum = monthCount, Title = "Past Month"},
                new IncomeStatisticItem { Sum = yearCount, Title = "Past Year"}
            };
        }

        private void UpdateIncomeByDateSummary()
        {
            var inclusiveEndDate = IncomeByDateEnd.AddDays(1);
            var totalValueMembershipsPaidWithinDateRange = 0m;
            var query = unitOfWork.MembershipRepository.Get(x => x.PaymentDate >= IncomeByDateStart && x.PaymentDate <= inclusiveEndDate);

            if (query.Any(x => x.PaymentReceived))
            {
                totalValueMembershipsPaidWithinDateRange = query.Sum(x => x.PurchasePrice);
            }

            IncomeByDateSummary = string.Format("Total value of memberships paid between {0} - {1} = {2:C2}", IncomeByDateStart.ToShortDateString(), IncomeByDateEnd.ToShortDateString(), totalValueMembershipsPaidWithinDateRange);
        }

        private void InitializeIncomeByDateSummary()
        {
            IncomeByDateStart = DateTime.Today.AddDays(-7);
            IncomeByDateEnd = DateTime.Today;
        }
    }
}
