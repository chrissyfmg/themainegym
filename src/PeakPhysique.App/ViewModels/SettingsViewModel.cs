﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GymMembers.App.Models;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure;

namespace GymMembers.App.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        private readonly IUnitOfWork unitOfWork;
        public RelayCommand AddDiscountType { get; private set; }
        public RelayCommand<int> DeleteDiscountTypeCommand { get; private set; }
        public RelayCommand SaveSettingsCommand { get; set; }
        public RelayCommand SendEraseCardMessageCommand { get; set; }

        private DiscountType newDiscountType;

        public DiscountType NewDiscountType
        {
            get { return newDiscountType; }
            set
            {
                if (newDiscountType == value) return;
                newDiscountType = value;
                RaisePropertyChanged();
            }
        }

        public SettingsViewModel(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            AllDiscountTypes = new ObservableCollection<DiscountType>(unitOfWork.DiscountTypeRepository.GetAll().Where(x => x.Deleted == false));
            AllMembershipTypes = new ObservableCollection<MembershipType>(unitOfWork.MembershipTypeRepository.GetAll());

            AddDiscountType = new RelayCommand(CreateDiscountType);
            DeleteDiscountTypeCommand = new RelayCommand<int>(DeleteDiscountType);
            NewDiscountType = new DiscountType();
            SaveSettingsCommand = new RelayCommand(SaveSettings);
            SendEraseCardMessageCommand = new RelayCommand(SendEraseCardMessage);
        }

        private void SendEraseCardMessage()
        {
            var message = new ChangeReaderStateMessage
            {
                NewState = NfcState.Erase
            };

            Messenger.Default.Send(message);
        }

        private ObservableCollection<DiscountType> allDiscountTypes;
        public ObservableCollection<DiscountType> AllDiscountTypes
        {
            get
            {
                return allDiscountTypes;
            }
            set
            {
                if (allDiscountTypes != value)
                {
                    allDiscountTypes = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<MembershipType> allMembershipTypes;
        public ObservableCollection<MembershipType> AllMembershipTypes
        {
            get { return allMembershipTypes; }
            set
            {
                if (allMembershipTypes != value)
                {
                    allMembershipTypes = value;
                    RaisePropertyChanged();
                }
            }
        }

        private void CreateDiscountType()
        {
            AllDiscountTypes.Add(NewDiscountType);

            unitOfWork.DiscountTypeRepository.Add(NewDiscountType);

            try
            {
                unitOfWork.SaveChanges();
            }
            catch (Exception exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to create discount", NotificationType = NotificationType.Error });
            }
            

            NewDiscountType = new DiscountType();
        }

        private void DeleteDiscountType(int discountTypeId)
        {
            unitOfWork.DiscountTypeRepository.Delete(x => x.Id == discountTypeId);

            var discountType = AllDiscountTypes.Single(x => x.Id == discountTypeId);
            AllDiscountTypes.Remove(discountType);

            try
            {
                unitOfWork.SaveChanges();
            }
            catch(Exception exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to delete discount", NotificationType = NotificationType.Error });
            }
            
        }

        private void SetDiscountsAsExpired(int discountTypeId)
        {
            var applicableDiscounts =
                unitOfWork.DiscountRepository.GetAll().Where(x => x.DiscountTypeId == discountTypeId);

            foreach (var discount in applicableDiscounts)
            {
                discount.ExpiryDate = DateTime.UtcNow.ToLocalTime();
            }
        }

        private void SaveSettings()
        {
            try
            {
                unitOfWork.SaveChanges();
                Messenger.Default.Send(new Notification { Message = "Settings saved successfully", NotificationType = NotificationType.Success });
            }
            catch (Exception exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to update settings", NotificationType = NotificationType.Error });
            }
            
        }
    }
}
