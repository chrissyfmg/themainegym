using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Timers;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GymMembers.App.Models;
using Timer = System.Timers.Timer;

namespace GymMembers.App.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public IList<PageLink> PageLinks { get; private set; }
        public SessionManager SessionManager { get; private set; }

        public RelayCommand ManualSessionControlCommand { get; set; }

        public MainViewModel()
        {
            InitializePages();
            RegisterMessages();
            SessionManager = new SessionManager();

            ManualSessionControlCommand = new RelayCommand(() =>
            {
                SessionManager.ManageSession(ManualCustomerNumber);
                ManualCustomerNumber = null;
            });

            NotificationTimer = new Timer(2000);
            NotificationTimer.Elapsed += OnTimedEvent;
        }

        private void RegisterMessages()
        {
            Messenger.Default.Register<NavigationMessage>(this, "GoToPage", GoToPage);
            Messenger.Default.Register<Notification>(this, DisplayNotification);
        }

        private void DisplayNotification(Notification notification)
        {
            notification.NotificationIconUrl =
                new Uri(string.Format(CultureInfo.InvariantCulture, "/Assets/Images/Icons/{0}.png",
                    notification.NotificationType), UriKind.Relative);
            NotificationMessage = notification;

            ShowNotification = true;
            NotificationTimer.Start();
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            NotificationTimer.Enabled = false;
            ShowNotification = false;
        }

        private void InitializePages()
        {
            PageLinks = new PageLinkProvider().Links.OrderBy(x => x.Order).ToList();

            if (PageLinks.Count > 0)
            {
                CurrentPageIndex = PageLinks.IndexOf(PageLinks.Single(x => x.IsInitialPage));
            }
        }

        private Timer notificationTimer;
        public Timer NotificationTimer
        {
            get { return notificationTimer; }

            set
            {
                if (notificationTimer != value)
                {
                    notificationTimer = value;
                }
            }
        }

        private Notification notificationMessage;
        public Notification NotificationMessage
        {
            get { return notificationMessage; }

            set
            {
                if (notificationMessage != value)
                {
                    notificationMessage = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool showNotification;
        public bool ShowNotification
        {
            get { return showNotification; }

            set
            {
                if (showNotification != value)
                {
                    showNotification = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Uri currentPageUrl;
        public Uri CurrentPageUrl
        {
            get { return currentPageUrl; }

            set
            {
                if (currentPageUrl != value)
                {
                    currentPageUrl = value;
                    RaisePropertyChanged();
                }
            }
        }

        private int currentPageIndex = -1;
        public int CurrentPageIndex
        {
            get { return currentPageIndex; }

            set
            {
                if (currentPageIndex != value)
                {
                    currentPageIndex = value;
                    CurrentPageUrl = PageLinks[value].PageUri;
                    RaisePropertyChanged();
                }
            }
        }

        private string manualCustomerNumber;

        public string ManualCustomerNumber
        {
            get { return manualCustomerNumber; }
            set
            {
                if (manualCustomerNumber != value)
                {
                    manualCustomerNumber = value;
                    RaisePropertyChanged();
                }
            }
        }

        private void GoToPage(NavigationMessage navigationMessage)
        {
            var page = PageLinks.FirstOrDefault(x => x.Name == navigationMessage.PageName);

            if (page != null)
            {
                CurrentPageUrl = page.PageUri;
                CurrentPageIndex = PageLinks.IndexOf(page);
            }
        }
    }
}