﻿using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GymMembers.App.Models;
using GymMembers.App.Virtualization;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure;

namespace GymMembers.App.ViewModels
{
    public class WhosHereViewModel : ViewModelBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly QueryableItemsProvider<Customer> customerProvider;

        public WhosHereViewModel(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            customerProvider = new QueryableItemsProvider<Customer>();

            RegisterMessages();
            CreateCommands();

            UpdateCustomerList(true);
        }

        private void CreateCommands()
        {
            ClearSearchCommand = new RelayCommand(ClearSearch);
        }

        private void RegisterMessages()
        {
            Messenger.Default.Register<SessionManagementMessage>(this, "UpdateCustomerList", UpdateCurrentSessionList);
        }

        private void UpdateCustomerList(bool selectFirst = false)
        {
            UpdateCustomerQuery();
            RefreshCustomerList();

            if (selectFirst)
            {
                SelectedCustomer = PresentCustomerCollection.FirstOrDefault();
            }
        }

        private void UpdateCustomerQuery()
        {
            var customerQuery = unitOfWork.CustomerRepository.Queryable().Where(x => x.Sessions.Any(s => s.EndTime == null));

            if (!string.IsNullOrWhiteSpace(SearchValue))
            {
                customerQuery = customerQuery.Where(x => x.FirstName.Contains(SearchValue)
                                                         || x.LastName.Contains(SearchValue)
                                                         || x.Number.ToString().Contains(SearchValue));
            }

            customerProvider.Queryable = customerQuery;
        }

        private void RefreshCustomerList()
        {
            PresentCustomerCollection = new VirtualizingCollection<Customer>(customerProvider, 10);
        }

        private void ClearSearch()
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                SearchValue = string.Empty;
            }
        }

        private void UpdateCurrentSessionList(SessionManagementMessage sessionManagementMessage)
        {
            if (sessionManagementMessage.SignedOut)
            {
                if (SelectedCustomer != null && SelectedCustomer.Number == sessionManagementMessage.CustomerNumber)
                {
                    SelectedCustomer = null;
                }
            }

            UpdateCustomerList();
        }

        public void RefreshCustomerSessions()
        {
            CustomerSessions = SelectedCustomer != null && SelectedCustomer.Sessions != null ? new ObservableCollection<Session>(SelectedCustomer.Sessions.OrderByDescending(x => x.StartTime)) : null;
        }

        public RelayCommand ClearSearchCommand { get; set; }

        private VirtualizingCollection<Customer> presentCustomerCollection;
        public VirtualizingCollection<Customer> PresentCustomerCollection
        {
            get { return presentCustomerCollection; }
            set
            {
                if (presentCustomerCollection != value)
                {
                    presentCustomerCollection = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<Session> customerSessions;
        public ObservableCollection<Session> CustomerSessions
        {
            get { return customerSessions; }
            set
            {
                if (customerSessions != value)
                {
                    customerSessions = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string searchValue;
        public string SearchValue
        {
            get { return searchValue; }
            set
            {
                searchValue = value;
                SelectedCustomer = null;
                UpdateCustomerList();
                RaisePropertyChanged();
            }
        }

        private Customer selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                RefreshCustomerSessions();
                RaisePropertyChanged();
            }
        }
    }
}
