﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GymMembers.App.Models;
using GymMembers.App.Pages;
using GymMembers.Domain.Extensions;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure;

namespace GymMembers.App.ViewModels
{
    public class CustomerRegistrationViewModel : ViewModelBase
    {
        private readonly IUnitOfWork unitOfWork;

        public CustomerRegistrationViewModel(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            Messenger.Default.Register<CameraStateMessage>(this, HandleCameraStateMessage);

            MembershipTypes = unitOfWork.MembershipTypeRepository.GetAll().Where(x => x.Active).ToList();
            DiscountTypes = unitOfWork.DiscountTypeRepository.GetAll().Where(x => !x.Deleted).ToList();
            Genders = unitOfWork.GenderRepository.GetAll().ToList();

            Customer = new Customer
            {
                DateOfBirth = DateTime.UtcNow.ToLocalTime(),
                PostalAddress = new PostalAddress()
            };

            DiscountExpiryDate = DateTime.Today.AddDays(1);

            SubmitForm = new RelayCommand(SubmitRegistration);
            DeleteDiscountCommand = new RelayCommand(DeleteDiscount);
            RegisterCardCommand = new RelayCommand(SendRegisterCardMessage);
        }

        private void SendRegisterCardMessage()
        {
            Messenger.Default.Send(new ChangeReaderStateMessage
            {
                CustomerNumber = Customer.Number,
                NewState = NfcState.Register
            });
        }

        private decimal calculatedDuePrice;
        public decimal CalculatedDuePrice
        {
            get { return calculatedDuePrice; }
            set
            {
                if (calculatedDuePrice == value) return;
                calculatedDuePrice = value;
                RaisePropertyChanged();
            }
        }

        private List<Gender> genders;
        public List<Gender> Genders
        {
            get { return genders; }
            set
            {
                if (genders == value) return;
                genders = value;
                RaisePropertyChanged();
            }
        }

        public List<MembershipType> MembershipTypes { get; private set; }
        public List<DiscountType> DiscountTypes { get; private set; }

        private DiscountType selectedDiscountType;
        public DiscountType SelectedDiscountType
        {
            get { return selectedDiscountType; }
            set
            {
                if (selectedDiscountType == value) return;
                selectedDiscountType = value;

                CalculateDuePrice();
                RaisePropertyChanged();
            }
        }

        private MembershipType selectedMembershipType;
        public MembershipType SelectedMembershipType
        {
            get { return selectedMembershipType; }
            set
            {
                if (selectedMembershipType == value) return;
                selectedMembershipType = value;

                CalculateDuePrice();
                RaisePropertyChanged();
            }
        }

        private DateTime discountExpiryDate;
        public DateTime DiscountExpiryDate
        {
            get { return discountExpiryDate; }
            set
            {
                if (discountExpiryDate == value) return;
                discountExpiryDate = value;
            }
        }

        private Customer customer;
        public Customer Customer
        {
            get
            {
                return customer;
            }
            set
            {
                if (customer == value) return;
                customer = value;
            }
        }

        private string webcamLblMessage;
        public string WebcamLblMessage
        {
            get { return webcamLblMessage; }
            set
            {
                if (webcamLblMessage == value) return;
                webcamLblMessage = value;
                RaisePropertyChanged();
            }
        }

        private BitmapImage imageUrl;
        public BitmapImage ImageUrl
        {
            get { return imageUrl; }
            set
            {
                if (imageUrl == value) return;
                imageUrl = value;
                RaisePropertyChanged();
            }
        }

        private bool paymentReceived;
        public bool PaymentReceived
        {
            get { return paymentReceived; }
            set
            {
                if (paymentReceived == value) return;
                paymentReceived = value;
            }
        }

        private bool showCameraErrorMessage;
        public bool ShowCameraErrorMessage
        {
            get { return showCameraErrorMessage; }
            set
            {
                if (showCameraErrorMessage != value)
                {
                    showCameraErrorMessage = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool showCameraPreview;
        public bool ShowCameraPreview
        {
            get { return showCameraPreview; }
            set
            {
                if (showCameraPreview != value)
                {
                    showCameraPreview = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool showTakenSnapshot;
        public bool ShowTakenSnapshot
        {
            get { return showTakenSnapshot; }
            set
            {
                if (showTakenSnapshot != value)
                {
                    showTakenSnapshot = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand SubmitForm
        {
            get;
            private set;
        }

        public RelayCommand DeleteDiscountCommand
        {
            get;
            private set;
        }

        public RelayCommand RegisterCardCommand { get; private set; }

        private void HandleCameraStateMessage(CameraStateMessage message)
        {
            switch (message.State)
            {
                case CameraState.Error:
                    WebcamLblMessage = message.Data;
                    ShowCameraErrorMessage = true;
                    ShowCameraPreview = false;
                    ShowTakenSnapshot = false;
                    break;

                case CameraState.Preview:
                    ShowCameraErrorMessage = false;
                    ShowCameraPreview = true;
                    ShowTakenSnapshot = false;
                    break;

                case CameraState.Taken:
                    SetImageUrl(message.Data);
                    ShowCameraErrorMessage = false;
                    ShowCameraPreview = false;
                    ShowTakenSnapshot = true;
                    break;
            }
        }

        private void SubmitRegistration()
        {
            if (selectedDiscountType != null)
            {
                var discount = new Discount
                    {
                        DiscountType = selectedDiscountType,
                        ExpiryDate = discountExpiryDate
                    };

                Customer.Discount = discount;
            }


            Customer.ImagePath = ImageUrl != null ? ImageUrl.UriSource.LocalPath : null;
            Customer.AddMembership(selectedMembershipType, PaymentReceived);
            unitOfWork.CustomerRepository.Add(Customer);
            unitOfWork.SaveChanges();

            GoToCustomerProfile(Customer.Id);
        }

        private void SetImageUrl(string path)
        {
            ImageUrl = new BitmapImage();
            ImageUrl.BeginInit();
            ImageUrl.CacheOption = BitmapCacheOption.OnLoad;
            ImageUrl.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            ImageUrl.UriSource = new Uri(path, UriKind.Absolute);
            ImageUrl.EndInit();
        }

        public void GoToCustomerProfile(int id)
        {
            var message = new NavigationMessage(id, typeof(AllCustomers));
            Messenger.Default.Send(message, "GoToPage");
        }

        private void CalculateDuePrice()
        {
            var purchasePrice = SelectedMembershipType != null
                ? SelectedMembershipType.Price.MinusPercentage(SelectedDiscountType != null ? SelectedDiscountType.ReductionPercentage : 0)
                : 0;

            CalculatedDuePrice = purchasePrice;
        }

        private void DeleteDiscount()
        {
            SelectedDiscountType = null;
        }
    }
}
