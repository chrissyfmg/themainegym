﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GymMembers.App.Models;
using GymMembers.App.Virtualization;
using GymMembers.Domain.Extensions;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure;

namespace GymMembers.App.ViewModels
{
    public class AllCustomersViewModel : ViewModelBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly QueryableItemsProvider<Customer> customerProvider;

        public AllCustomersViewModel(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            customerProvider = new QueryableItemsProvider<Customer>();

            InitializeData();
            CreateCommands();
            UpdateCustomerList(true);
        }

        private void InitializeData()
        {
            Genders = unitOfWork.GenderRepository.GetAll().ToList();
            DiscountTypes = unitOfWork.DiscountTypeRepository.Get(x => !x.Deleted).ToList();
            MembershipTypes = unitOfWork.MembershipTypeRepository.GetAll().Where(x => x.Active).ToList();
            SelectedDiscountType = DiscountTypes.FirstOrDefault();
            SelectedMembershipType = MembershipTypes.FirstOrDefault();
            NewDiscountExpiry = DateTime.Today.AddDays(1);
        }

        private void CreateCommands()
        {
            ClearSearchCommand = new RelayCommand(ClearSearch);
            ApplyDiscountCommand = new RelayCommand(ApplyDiscount);
            SendRegisterCardMessageCommand = new RelayCommand(SendRegisterCardMessage);
            DeleteDiscountCommand = new RelayCommand(DeleteDiscount);
            UpdateProfileCommand = new RelayCommand(UpdateUserProfile);
            DeleteMembershipCommand = new RelayCommand<int>(DeleteMembership);
            AddMembershipCommand = new RelayCommand(AddCustomerMembership);
        }

        private void UpdateCustomerQuery()
        {
            var customerQuery = unitOfWork.CustomerRepository.Queryable();

            if (!string.IsNullOrWhiteSpace(SearchValue))
            {
                customerQuery = customerQuery.Where(x => x.FirstName.Contains(SearchValue)
                                                         || x.LastName.Contains(SearchValue)
                                                         || x.Number.ToString().Contains(SearchValue));
            }
            else
            {
                customerQuery = customerQuery.OrderByDescending(x => x.Number);
            }

            if (ShowExpiredMemberships)
            {
                var tomorrow = DateTime.Today.AddDays(1);

                customerQuery = 
                    customerQuery.Where(x => !x.Memberships.Any() 
                        || !x.Memberships.Any(y => y.EndDate >= tomorrow));
            }

            if (OrderByLowUsage)
            {
                customerQuery = customerQuery.OrderByDescending(x => x.Memberships.Count)
                                                .ThenBy(x => x.Sessions.Count)
                                                .ThenByDescending(x => x.Number);
            }

            customerProvider.Queryable = customerQuery;
        }

        private void RefreshCustomerList()
        {
            CustomerCollection = new VirtualizingCollection<Customer>(customerProvider, 10);
        }

        private void UpdateCustomerList(bool selectFirst = false)
        {
            UpdateCustomerQuery();
            RefreshCustomerList();

            if (selectFirst)
            {
                SelectedCustomer = CustomerCollection.FirstOrDefault();
            }
        }
        
        private void CalculateDuePrice()
        {
            if (SelectedMembershipType != null)
            {
                CalculatedDuePrice = SelectedCustomer == null || SelectedCustomer.Discount == null
                    ? SelectedMembershipType.Price
                    : SelectedMembershipType.Price.MinusPercentage(SelectedCustomer.Discount.DiscountType.ReductionPercentage);
            }
        }

        private void RefreshMemberships()
        {
            CustomerMemberships = SelectedCustomer != null ? new ObservableCollection<Membership>(SelectedCustomer.Memberships) : null;
        }

        #region Command Methods

        private void AddCustomerMembership()
        {
            try
            {
                SelectedCustomer.AddMembership(SelectedMembershipType, MembershipPaymentReceived);
                unitOfWork.SaveChanges();

                RefreshMemberships();
                UpdateCustomerList();
            }
            catch (Exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to create membership", NotificationType = NotificationType.Error });
            }

            NewMembership = new Membership();
        }

        private void DeleteDiscount()
        {
            try
            {
                SelectedCustomer.Discount = null;
                unitOfWork.SaveChanges();

                CalculateDuePrice();
            }
            catch (Exception exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to delete discount", NotificationType = NotificationType.Error });
            }
        }

        private void UpdateUserProfile()
        {
            try
            {
                unitOfWork.SaveChanges();
                UpdateCustomerList();

                Messenger.Default.Send(new Notification { Message = "Profile updated successfully", NotificationType = NotificationType.Success });
            }
            catch (Exception exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to update profile", NotificationType = NotificationType.Error });
            }
        }

        private void DeleteMembership(int id)
        {
            try
            {
                var membership = CustomerMemberships.SingleOrDefault(x => x.Id == id);

                unitOfWork.MembershipRepository.Delete(membership);
                CustomerMemberships.Remove(membership);
                unitOfWork.SaveChanges();
                UpdateCustomerList();
                RefreshMemberships();
            }
            catch (Exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to delete membership", NotificationType = NotificationType.Error });
            }
        }

        private void SendRegisterCardMessage()
        {
            try
            {
                var message = new ChangeReaderStateMessage
                {
                    CustomerNumber = SelectedCustomer.Number,
                    NewState = NfcState.Register
                };

                Messenger.Default.Send(message);
            }
            catch (Exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to register card", NotificationType = NotificationType.Error });
            }
        }

        private void ClearSearch()
        {
            if (!string.IsNullOrEmpty(SearchValue))
            {
                SearchValue = string.Empty;
            }
        }

        private void ApplyDiscount()
        {
            try
            {
                var discount = new Discount
                {
                    DiscountType = SelectedDiscountType,
                    ExpiryDate = NewDiscountExpiry
                };

                SelectedCustomer.Discount = discount;
                CalculateDuePrice();

                unitOfWork.SaveChanges();
            }
            catch (Exception)
            {
                Messenger.Default.Send(new Notification { Message = "Unable to apply discount", NotificationType = NotificationType.Error });
            }
        }

        #endregion  

        #region Complex Properties

        private Customer selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                if (SelectedCustomer != null)
                {
                    selectedCustomer.CheckDiscountEntitlement();
                }
                
                RefreshMemberships();
                CalculateDuePrice();
                RaisePropertyChanged();
            }
        }

        private string searchValue;
        public string SearchValue
        {
            get { return searchValue; }
            set
            {
                searchValue = value;
                SelectedCustomer = null;
                UpdateCustomerList();
                RaisePropertyChanged();
            }
        }

        private bool orderByLowUsage;
        public bool OrderByLowUsage
        {
            get { return orderByLowUsage; }
            set
            {
                if (orderByLowUsage != value)
                {
                    orderByLowUsage = value;
                    UpdateCustomerList();
                    RaisePropertyChanged();
                }
            }
        }

        private bool showExpiredMemberships;
        public bool ShowExpiredMemberships
        {
            get { return showExpiredMemberships; }
            set
            {
                if (showExpiredMemberships != value)
                {
                    showExpiredMemberships = value;
                    UpdateCustomerList();
                    RaisePropertyChanged();
                }
            }
        }

        private MembershipType selectedMembershipType;
        public MembershipType SelectedMembershipType
        {
            get { return selectedMembershipType; }
            set
            {
                if (selectedMembershipType != value)
                {
                    selectedMembershipType = value;
                    CalculateDuePrice();
                    RaisePropertyChanged();
                }
            }
        }

        private decimal calculatedDuePrice;
        public decimal CalculatedDuePrice
        {
            get { return calculatedDuePrice; }
            set
            {
                if (calculatedDuePrice == value) return;
                calculatedDuePrice = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Command Properties

        public RelayCommand ApplyDiscountCommand { get; set; }
        public RelayCommand ClearSearchCommand { get; set; }
        public RelayCommand DeleteDiscountCommand { get; set; }
        public RelayCommand UpdateProfileCommand { get; set; }
        public RelayCommand<int> DeleteMembershipCommand { get; private set; }
        public RelayCommand AddMembershipCommand { get; set; }
        public RelayCommand SendRegisterCardMessageCommand { get; set; }

        #endregion

        #region Basic Properties

        private bool membershipPaymentReceived;
        public bool MembershipPaymentReceived
        {
            get { return membershipPaymentReceived; }
            set
            {
                if (membershipPaymentReceived != value)
                {
                    membershipPaymentReceived = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<MembershipType> membershipTypes;
        public List<MembershipType> MembershipTypes
        {
            get { return membershipTypes; }
            set
            {
                if (membershipTypes != value)
                {
                    membershipTypes = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Membership newMembership;
        public Membership NewMembership
        {
            get { return newMembership; }
            set
            {
                if (newMembership != value)
                {
                    newMembership = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<Membership> customerMemberships;
        public ObservableCollection<Membership> CustomerMemberships
        {
            get { return customerMemberships; }
            set
            {
                if (customerMemberships != value)
                {
                    customerMemberships = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<Gender> genders;
        public List<Gender> Genders
        {
            get { return genders; }
            set
            {
                if (genders != value)
                {
                    genders = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<DiscountType> discountTypes;
        public List<DiscountType> DiscountTypes
        {
            get { return discountTypes; }
            set
            {
                if (discountTypes != value)
                {
                    discountTypes = value;
                    RaisePropertyChanged();
                }
            }
        }

        private DateTime newDiscountExpiry;
        public DateTime NewDiscountExpiry
        {
            get { return newDiscountExpiry; }
            set
            {
                if (newDiscountExpiry != value)
                {
                    newDiscountExpiry = value;
                    RaisePropertyChanged();
                }
            }
        }

        private DiscountType selectedDiscountType;
        public DiscountType SelectedDiscountType
        {
            get { return selectedDiscountType; }
            set
            {
                if (selectedDiscountType != value)
                {
                    selectedDiscountType = value;
                    RaisePropertyChanged();
                }
            }
        }

        private VirtualizingCollection<Customer> customerCollection;
        public VirtualizingCollection<Customer> CustomerCollection
        {
            get { return customerCollection; }
            set
            {
                customerCollection = value;
                RaisePropertyChanged();
            }
        }

        #endregion
    }
}