using System.Reflection;
using CommonServiceLocator.NinjectAdapter.Unofficial;
using Microsoft.Practices.ServiceLocation;
using Ninject;

namespace GymMembers.App.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            InitializeNinject();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public CustomerRegistrationViewModel CustomerRegistration
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CustomerRegistrationViewModel>();
            }
        }

        public SettingsViewModel Settings
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SettingsViewModel>();
            }
        }

        public AllCustomersViewModel AllCustomers
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AllCustomersViewModel>();
            }
        }

        public StatisticsViewModel Statistics
        {
            get
            {
                return ServiceLocator.Current.GetInstance<StatisticsViewModel>();
            }
        }

        public DueMembershipsViewModel DueMemberships
        {
            get
            {
                return ServiceLocator.Current.GetInstance<DueMembershipsViewModel>();
            }
        }

        public WhosHereViewModel WhosHere
        {
            get
            {
                return ServiceLocator.Current.GetInstance<WhosHereViewModel>();
            }
        }

        private static void InitializeNinject()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(kernel));
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}