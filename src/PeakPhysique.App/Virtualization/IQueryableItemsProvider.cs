﻿using System.Collections.Generic;
using System.Linq;
using GymMembers.Domain;

namespace GymMembers.App.Virtualization
{
    internal class QueryableItemsProvider<T> : IItemsProvider<T> where T : Entity
    {
        public IQueryable<T> Queryable { get; set; }

        public QueryableItemsProvider(IQueryable<T> queryable)
        {
            Queryable = queryable;
        }

        public QueryableItemsProvider()
        {
            
        }

        public int FetchCount()
        {
            return Queryable.Count();
        }

        public IList<T> FetchRange(int startIndex, int count)
        {
            if (!(Queryable.Expression.Type == typeof(IOrderedQueryable<T>)))
            {
                Queryable = Queryable.OrderBy(x => x.Id);
            }
            return Queryable.Skip(startIndex).Take(count).ToArray();
        }
    }
}
