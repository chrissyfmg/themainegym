﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Expression.Encoder.Devices;
using GymMembers.App.Attributes;
using GymMembers.App.Models;

namespace GymMembers.App.Pages
{
    /// <summary>
    /// Interaction logic for CustomerRegistration.xaml
    /// </summary>
    [PageSettings(4, "PersonAdd", "Registration")]
    public partial class CustomerRegistration : Page
    {
        public CustomerRegistration()
        {
            InitializeComponent();
        }

        private void TakePicture(object sender, System.Windows.RoutedEventArgs e)
        {
            var fileName = LblCustomerNumber.Content.ToString();

            var fileLocation = Path.Combine(GetImageDirectory(), fileName + ".jpeg");
            
            if (File.Exists(fileLocation))
            {
                File.Delete(fileLocation);
            }

            WebcamCtrl.TakeSnapshot(fileName);
            WebcamCtrl.StopPreview();

            Messenger.Default.Send(new CameraStateMessage(CameraState.Taken, fileLocation));
        }

        private void StartWebcam(object sender, System.Windows.RoutedEventArgs e)
        {
            if (InitializeCamera())
            {
                WebcamCtrl.StartPreview();

                Messenger.Default.Send(new CameraStateMessage(CameraState.Preview));
            }
        }

        private bool InitializeCamera()
        {
            var vidDevices = EncoderDevices.FindDevices(EncoderDeviceType.Video);

            if (vidDevices.Count > 0)
            {
                WebcamCtrl.VideoDevice = vidDevices.FirstOrDefault();
                WebcamCtrl.ImageDirectory = GetImageDirectory();
                return true;
            }

            Messenger.Default.Send(new CameraStateMessage(CameraState.Error, "No webcam found"));
            return false;
        }

        public string GetImageDirectory()
        {
            var profileImageFolder = ConfigurationManager.AppSettings["ProfileImageFolder"];
            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            var assemblyFolder = Path.GetDirectoryName(assemblyLocation);

            var profileImagesDirectory = Path.Combine(assemblyFolder, profileImageFolder);

            if (!Directory.Exists(profileImagesDirectory))
                Directory.CreateDirectory(profileImagesDirectory);

            return profileImagesDirectory;
        }
    }
}
