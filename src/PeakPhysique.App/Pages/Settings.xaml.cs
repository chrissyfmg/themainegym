﻿using System.Windows.Controls;
using GymMembers.App.Attributes;

namespace GymMembers.App.Pages
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    [PageSettings(5, "Settings")]
    public partial class Settings : Page
    {
        public Settings()
        {
            InitializeComponent();
        }
    }
}
