﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text.RegularExpressions;
using GymMembers.App.Attributes;
using GymMembers.App.Models;

namespace GymMembers.App
{
    internal class PageLinkProvider
    {
        public PageLinkProvider()
        {
            Links = BuildPageLinks();
        }

        private static IReadOnlyCollection<PageLink> BuildPageLinks()
        {
            var pages = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.Namespace == "GymMembers.App.Pages");
            var pageLinks = new List<PageLink>();

            foreach (var page in pages)
            {
                var settings = (PageSettingsAttribute)page.GetCustomAttributes(false).SingleOrDefault(x => x is PageSettingsAttribute);

                if (settings == null)
                {
                    throw new InvalidOperationException(string.Format("The {0} page requires a setting attribute", page.Name));
                }

                pageLinks.Add(new PageLink
                {
                    IsInitialPage = settings.IsDefault,
                    Order = settings.Order,
                    Name = page.Name,
                    DisplayName = settings.DisplayName ?? Regex.Replace(page.Name, "(\\B[A-Z,0-9])", " $1"),
                    PageUri = new Uri(string.Format(CultureInfo.InvariantCulture, "Pages/{0}.xaml", page.Name), UriKind.Relative),
                    IconUri = new Uri(string.Format(CultureInfo.InvariantCulture, "/Assets/Images/Icons/{0}.png", settings.IconName), UriKind.Relative)
                });
            }

            return pageLinks.AsReadOnly();
        }

        public IReadOnlyCollection<PageLink> Links { get; private set; }
    }
}
