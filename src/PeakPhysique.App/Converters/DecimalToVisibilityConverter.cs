﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace GymMembers.App.Converters
{
    public class DecimalToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var amount = (decimal) value;
            var visibleWhenLessThanOrEqualToZero = System.Convert.ToBoolean(parameter);

            if (visibleWhenLessThanOrEqualToZero)
            {
                return amount <= 0 ? Visibility.Visible : Visibility.Collapsed;
            }

            return amount > 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}