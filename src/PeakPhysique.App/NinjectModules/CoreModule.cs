﻿using GalaSoft.MvvmLight;
using NfcManager;
using Ninject.Modules;
using GymMembers.Domain;
using GymMembers.Infrastructure;
using GymMembers.Infrastructure.Providers;

namespace GymMembers.App.NinjectModules
{
    public class CoreModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IUnitOfWork>().To<UnitOfWork>().When(x => !ViewModelBase.IsInDesignModeStatic);
            Kernel.Bind<IUnitOfWork>().To<MockUnitOfWork>().When(x => ViewModelBase.IsInDesignModeStatic);

            Kernel.Bind<ICustomerNumberProvider>().To<CustomerNumberProvider>().When(x => !ViewModelBase.IsInDesignModeStatic);
            Kernel.Bind<ICustomerNumberProvider>().To<MockCustomerNumberProvider>().When(x => ViewModelBase.IsInDesignModeStatic);

            Kernel.Bind<SmartCardManager>().ToSelf().InSingletonScope();
        }
    }
}
