﻿using Ninject.Modules;
using GymMembers.App.ViewModels;

namespace GymMembers.App.NinjectModules
{
    public class ViewModelModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<MainViewModel>().ToSelf();
            Kernel.Bind<AllCustomersViewModel>().ToSelf();
            Kernel.Bind<SettingsViewModel>().ToSelf();
            Kernel.Bind<StatisticsViewModel>().ToSelf();
            Kernel.Bind<DueMembershipsViewModel>().ToSelf();
            Kernel.Bind<WhosHereViewModel>().ToSelf();
        } 
    }
}