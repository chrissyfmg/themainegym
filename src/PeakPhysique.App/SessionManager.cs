﻿using System;
using System.Linq;
using System.Text;
using System.Timers;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using NfcManager;
using GymMembers.App.Models;
using GymMembers.Domain.Models;
using GymMembers.Infrastructure;

namespace GymMembers.App
{
    public class SessionManager : ObservableObject
    {
        private SmartCardManager nfcManager;
        private readonly Timer stateSecondTimer;
        private const int StateResetSeconds = 5;

        private readonly Timer profileResetTimer;
        private const int ProfileResetSeconds = 5;
        private int secondsUntilProfileReset;

        private int nextCustomerNumber;

        private long nextAllowedSessionTicks = DateTime.UtcNow.Ticks;


        public bool ManualSessionEnabled
        {
            get { return NfcState == NfcState.Disconnected || NfcState == NfcState.Ready; }
        }

        private bool showStateResetTimer;
        public bool ShowStateResetTimer
        {
            get { return showStateResetTimer; }
            set
            {
                if (showStateResetTimer != value)
                {
                    showStateResetTimer = value;
                    RaisePropertyChanged();
                }
            }
        }

        private int secondsUntilStateReset;
        public int SecondsUntilStateReset
        {
            get { return secondsUntilStateReset; }
            set
            {
                if (secondsUntilStateReset != value)
                {
                    secondsUntilStateReset = value;
                    RaisePropertyChanged();
                }
            }
        }

        public SessionManager()
        {
            Messenger.Default.Register<ChangeReaderStateMessage>(this, ChangeReaderStateHandler);
            InitializeCardReader();

            stateSecondTimer = new Timer(1000);
            stateSecondTimer.Elapsed += StateSecondElapsed;

            profileResetTimer = new Timer(1000);
            profileResetTimer.Elapsed += ProfileSecondElapsed;

        }

        private void ProfileSecondElapsed(object sender, ElapsedEventArgs e)
        {
            secondsUntilProfileReset--;

            if (secondsUntilProfileReset == 0)
            {
                ResetProfile();
            }
        }

        void StateSecondElapsed(object sender, ElapsedEventArgs e)
        {
            SecondsUntilStateReset--;

            if (SecondsUntilStateReset == 0)
            {
                ResetState();
            }
        }

        private void ResetProfile()
        {
            UpdateMessageForState(NfcState);
            LastSessionManageCustomer = null;
            profileResetTimer.Stop();
        }

        private void ResetState()
        {
            if (NfcState != NfcState.Disconnected)
            {
                NfcState = NfcState.Ready;
            }

            ShowStateResetTimer = false;
            stateSecondTimer.Stop();
        }

        private void InitializeCardReader()
        {
            NfcState = NfcState.Disconnected;

            nfcManager = ServiceLocator.Current.GetInstance<SmartCardManager>();

            nfcManager.CardConnected += nfcManager_CardConnected;
            nfcManager.ReaderConnected += readerName => NfcState = NfcState.Ready;
            nfcManager.ReaderDisconnected += () =>
            {
                NfcState = NfcState.Disconnected;
                ShowStateResetTimer = false;
            };
            nfcManager.Start();
        }

        void nfcManager_CardConnected()
        {
            switch (NfcState)
            {
                case NfcState.Ready:
                    ReadNfc();
                    break;
                case NfcState.Register:
                    RegisterCard();
                    break;
                case NfcState.Erase:
                    EraseCard();
                    break;
            }
        }

        private void EraseCard()
        {
            var erased = nfcManager.UninitializeBlock(1);
            if (erased)
            {
                AlertOk();
                ResetState();
            }
            else
            {
                AlertInternalError();
            }
        }

        private void ReadNfc()
        {
            var response = nfcManager.ReadString(1);
            if (response.Success)
            {
                ManageSession(response.Output);
            }
            else
            {
                AlertInternalError();
            }
        }

        public void ManageSession(string customerNumberString)
        {
            var currentTicks = DateTime.UtcNow.Ticks;
            if (nextAllowedSessionTicks < currentTicks)
            {
                int customerNumber;

                var validNumber = int.TryParse(customerNumberString, out customerNumber);

                if (validNumber)
                {
                    var unitOfWork = ServiceLocator.Current.GetInstance<IUnitOfWork>();
                    var customer = unitOfWork.CustomerRepository.Get(x => x.Number == customerNumber).FirstOrDefault();

                    if (customer != null)
                    {
                        var currentSession = customer.Sessions.SingleOrDefault(x => x.EndTime == null);
                        var signedOut = false;

                        if (currentSession != null)
                        {
                            currentSession.EndTime = DateTime.UtcNow.ToLocalTime();
                            Message = string.Format("{0} has been signed out", customer.FullName);
                            signedOut = true;
                            AlertOk();
                        }
                        else
                        {
                            Message = StartSession(customer);
                        }

                        unitOfWork.SaveChanges();

                        SendUpdateCustomerListMessage(signedOut, customer.Number);
                    }
                    else
                    {
                        Message = "No customer found";
                    }

                    LastSessionManageCustomer = customer;

                    var limitTicks = TimeSpan.FromMilliseconds(500).Ticks;
                    nextAllowedSessionTicks = currentTicks + limitTicks;
                }
                else
                {
                    AlertInternalError();
                }
            }
        }

        private void SendUpdateCustomerListMessage(bool hasSignedOut, int customerNumber)
        {
            var sessionManagementMessage = new SessionManagementMessage
            {
                SignedOut = hasSignedOut,
                CustomerNumber = customerNumber
            };

            Messenger.Default.Send(sessionManagementMessage, "UpdateCustomerList");
        }

        private string StartSession(Customer customer)
        {
            var paymentsUpToDate = MembershipsPaid(customer);
            var hasValidMembership = HasMembership(customer);

            if (hasValidMembership)
            {
                var session = new Session
                {
                    StartTime = DateTime.UtcNow.ToLocalTime()
                };

                customer.Sessions.Add(session);
            }

            if (paymentsUpToDate && hasValidMembership)
            {
                AlertOk();
            }
            else
            {
                AlertAttention();
            }

            return BuildSessionStartMessage(customer, paymentsUpToDate, hasValidMembership);
        }

        private bool HasMembership(Customer customer)
        {
            return customer.Memberships.Any(x => x.EndDate >= DateTime.Today.AddDays(1));
        }

        private bool MembershipsPaid(Customer customer)
        {
            if (customer.AmountOwed > 0)
            {
                return false;
            }

            return true;
        }

        private string BuildSessionStartMessage(Customer customer, bool paymentsUpToDate, bool hasValidMembership)
        {
            var builder = new StringBuilder();

            builder.AppendFormat("{0} ", customer.FullName);

            if (!paymentsUpToDate)
            {
                builder.AppendFormat("owes £{0:0.00}", customer.AmountOwed);

                if (!hasValidMembership)
                {
                    builder.AppendFormat(" and also requires a new membership before signing in");
                }
            }
            else if (!hasValidMembership)
            {
                builder.AppendFormat("requires a new membership before signing in");
            }
            else
            {
                builder.AppendFormat("has been signed in");
            }

            return builder.ToString();
        }

        private void RegisterCard()
        {
            var cardInitialized = nfcManager.InitializeBlock(1);
            if (cardInitialized)
            {
                var customerNumberWritten = nfcManager.WriteString(1, nextCustomerNumber.ToString());
                if (customerNumberWritten)
                {
                    nfcManager.Alert(false, 1);
                    ResetState();
                    return;
                }
            }

            nfcManager.Alert(true, 2);
        }

        private void ChangeReaderStateHandler(ChangeReaderStateMessage message)
        {
            if (ReaderStateChangedAllowed(message.NewState))
            {
                NfcState = message.NewState;

                if (message.NewState == NfcState.Register)
                {
                    nextCustomerNumber = message.CustomerNumber;
                }

                if (NfcState != NfcState.Ready || NfcState != NfcState.Disconnected)
                {
                    SecondsUntilStateReset = StateResetSeconds;
                    ShowStateResetTimer = true;
                    stateSecondTimer.Start();
                }
            }
        }

        private void UpdateMessageForState(NfcState state)
        {
            switch (state)
            {
                case NfcState.Disconnected:
                    Message = "No reader connected";
                    break;
                case NfcState.Ready:
                    Message = "Sign in/out";
                    break;
                case NfcState.Register:
                    Message = "Tag registration";
                    break;
                case NfcState.Erase:
                    Message = "Erase tag";
                    break;
                default:
                    Message = "Reader status unknown, contact support";
                    break;
            }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set
            {
                if (message != value)
                {
                    message = value;
                    RaisePropertyChanged();
                }
            }
        }

        private NfcState nfcState = NfcState.Disconnected;
        private NfcState NfcState
        {
            get
            {
                return nfcState;
            }
            set
            {
                nfcState = value;
                UpdateMessageForState(value);
                RaisePropertyChanged("ManualSessionEnabled");
            }
        }

        private Customer lastSessionManageCustomer;
        public Customer LastSessionManageCustomer
        {
            get { return lastSessionManageCustomer; }
            set
            {
                if (lastSessionManageCustomer != value)
                {
                    lastSessionManageCustomer = value;
                    RaisePropertyChanged();
                }

                if (value != null)
                {
                    secondsUntilProfileReset = ProfileResetSeconds;
                    profileResetTimer.Start();
                }
            }
        }

        private bool ReaderStateChangedAllowed(NfcState attemptedState)
        {
            if (attemptedState == NfcState.Erase || attemptedState == NfcState.Register)
            {
                return NfcState == NfcState.Ready;
            }

            return true;
        }

        private void AlertOk()
        {
            nfcManager.Alert(false, 1);
        }

        private void AlertAttention()
        {
            nfcManager.Alert(true, 4);
        }

        private void AlertInternalError()
        {
            nfcManager.Alert(true, 2);
        }
    }
}
