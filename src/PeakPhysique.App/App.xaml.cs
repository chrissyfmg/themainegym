﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace GymMembers.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            BackupDatabase();
            base.OnStartup(e);
        }

        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            EventLog log = new EventLog("Application");
            log.Source = "Gym Members";
            log.WriteEntry(string.Format("Message:{0} Stack:{1} Inner:{2}", e.Exception.Message, e.Exception.StackTrace, e.Exception.InnerException));
            e.Handled = true;
        }

        private void BackupDatabase()
        {
            var backupSourceFolderPath = ConfigurationManager.AppSettings.Get("BackupFolderLocation");
            var backupFileName = "GymMembers.sdf";

            var configBackupFileName = ConfigurationManager.AppSettings.Get("BackupFileName");

            if(!string.IsNullOrWhiteSpace(configBackupFileName))
            {
                backupFileName = configBackupFileName;
            }

            if (!string.IsNullOrWhiteSpace(backupSourceFolderPath))
            {
                var dbFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, backupFileName);

                if(File.Exists(dbFilePath))
                {
                    var backupFilePath = Path.Combine(backupSourceFolderPath, backupFileName + ".backup");
                    File.Copy(dbFilePath, backupFilePath, true);
                }
            }
        }
    }
}
