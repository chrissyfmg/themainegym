﻿using System;

namespace GymMembers.App.Attributes
{
    internal sealed class PageSettingsAttribute : Attribute
    {
        private readonly int order;
        private readonly string iconName;
        private readonly bool isDefault;
        private readonly string displayName;

        public PageSettingsAttribute(int order, string iconName, string displayName = null, bool isDefault = false)
        {
            this.order = order;
            this.iconName = iconName;
            this.displayName = displayName;
            this.isDefault = isDefault;
        }

        public int Order { get { return order; }}
        public bool IsDefault { get { return isDefault; }}
        public string DisplayName { get { return displayName; } }
        public string IconName { get { return iconName; } }
    }
}
