﻿namespace GymMembers.Domain
{
    public interface ICustomerNumberProvider
    {
        int GetCustomerNumber();
    }
}
