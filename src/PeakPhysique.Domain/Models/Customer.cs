﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Practices.ServiceLocation;
using GymMembers.Domain.Extensions;


namespace GymMembers.Domain.Models
{
    public class Customer : Entity, INotifyPropertyChanged
    {
        public Customer()
        {
            Number = ServiceLocator.Current.GetInstance<ICustomerNumberProvider>().GetCustomerNumber();
        }

        public int Number { get; set; }
        public string ImagePath { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ContactNumber { get; set; }
        public virtual Gender Gender { get; set; }
        public string EmailAddress { get; set; }

        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }

        public decimal AmountOwed
        {
            get
            {
                if (Memberships == null)
                {
                    return 0m;
                }

                var overdueMemberships = Memberships.Where(x => !x.PaymentReceived).ToArray();
                return overdueMemberships.Any() ? overdueMemberships.Sum(y => y.PurchasePrice) : 0m;
            }
        }

        public string MembershipSummary
        {
            get
            {
                if (Memberships != null)
                {
                    var currentMembership = Memberships.OrderByDescending(x => x.EndDate).FirstOrDefault(x => x.EndDate >= DateTime.Today.AddDays(1));
                    if (currentMembership != null)
                    {
                        var daysLeft = GetMembershipTimeLeft(currentMembership.EndDate);

                        return string.Format("{0} membership expires on {1} ({2} Days)", currentMembership.MembershipType.Name, currentMembership.EndDate.ToShortDateString(), daysLeft);
                    }
                }

                return "No current membership";
            }
        }

        public virtual ICollection<Membership> Memberships { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }

        private Discount discount;
        public virtual Discount Discount
        {
            get
            {
                return discount;
            }
            set
            {
                discount = value;
                OnPropertyChanged();
            }
        }

        public virtual PostalAddress PostalAddress { get; set; }

        public void AddMembership(MembershipType type, bool paymentReceived)
        {
            Memberships = Memberships ?? new List<Membership>();

            var purchasePrice = Discount != null
                ? type.Price.MinusPercentage(Discount.DiscountType.ReductionPercentage)
                : type.Price;

            Memberships.Add(new Membership
            {
                MembershipTypeId = type.Id,
                PurchasePrice = purchasePrice,
                StartDate = DateTime.UtcNow.ToLocalTime(),
                EndDate = GetEndDate(type.Duration),
                PaymentReceived = paymentReceived
            });
        }

        private DateTime GetEndDate(MembershipDuration duration)
        {
            switch (duration)
            {
                case MembershipDuration.Daily:
                    return DateTime.Today.AddDays(1);
                case MembershipDuration.Weekly:
                    return DateTime.Today.AddDays(7);
                case MembershipDuration.Monthly:
                    return DateTime.Today.AddMonths(1);
                case MembershipDuration.Annually:
                    return DateTime.Today.AddYears(1);
                default:
                    throw new InvalidOperationException("No valid membership duration");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private int GetMembershipTimeLeft(DateTime endDate)
        {
            var today = DateTime.Today;
            
            return (endDate - today).Days;
        }

        public void CheckDiscountEntitlement()
        {
            if (Discount != null && Discount.ExpiryDate < DateTime.UtcNow.ToLocalTime())
            {
                Discount = null;
            }
        }
    }
}