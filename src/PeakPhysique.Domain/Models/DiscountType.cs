﻿using System.Collections.Generic;

namespace GymMembers.Domain.Models
{
    public class DiscountType : Entity
    {
        public string Name { get; set; }
        public int ReductionPercentage { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<Discount> Discounts { get; set; }
    }
}