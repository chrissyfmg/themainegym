﻿using System;

namespace GymMembers.Domain.Models
{
    public class Membership : Entity
    {
        public DateTime? PaymentDate { get; set; }
        public decimal PurchasePrice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Deleted { get; set; }

        public virtual Customer Customer { get; set; }

        public int MembershipTypeId { get; set; }
        public virtual MembershipType MembershipType { get; set; }

        private bool paymentReceived;

        public bool PaymentReceived
        {
            get
            {
                return paymentReceived;
            }
            set
            {
                paymentReceived = value;
                PaymentDate = value ? DateTime.UtcNow.ToLocalTime() : (DateTime?)null;
            }
        }
    }
}