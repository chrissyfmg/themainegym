﻿using System.Collections.Generic;

namespace GymMembers.Domain.Models
{
    public class MembershipType : Entity
    {
        public string Name { get; set; }
        public MembershipDuration Duration { get; set; }
        public decimal Price { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Membership> Memberships { get; set; }
    }
}