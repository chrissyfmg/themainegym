﻿namespace GymMembers.Domain.Models
{
    public class PostalAddress : Entity
    {
        public string FirstLine { get; set; }
        public string SecondLine { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }

        public virtual Customer Customer { get; set; }
    }
}