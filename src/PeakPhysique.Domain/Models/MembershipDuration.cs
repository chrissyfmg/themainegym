﻿namespace GymMembers.Domain.Models
{
    public enum MembershipDuration
    {
        Daily, 
        Weekly,
        Monthly,
        Annually
    }
}
