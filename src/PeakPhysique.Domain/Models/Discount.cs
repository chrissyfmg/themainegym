﻿using System;

namespace GymMembers.Domain.Models
{
    public class Discount : Entity
    {
        public DateTime ExpiryDate { get; set; }
        public virtual Customer Customer { get; set; }
        public int DiscountTypeId { get; set; }
        public virtual DiscountType DiscountType { get; set; }
    }
}