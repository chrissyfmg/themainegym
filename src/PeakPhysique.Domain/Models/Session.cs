﻿using System;

namespace GymMembers.Domain.Models
{
    public class Session : Entity
    {
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public virtual Customer Customer { get; set; }
    }
}