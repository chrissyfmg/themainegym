﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace GymMembers.Domain.Validators
{
    public class ExactLengthValidator : ValidationRule
    {
        public int Length { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var input = (string)value;
            if (input != null && input.Length != Length)
            {
                return new ValidationResult(false,
                  String.Format("Value must be exactly {0} characters", Length));
            }

            return new ValidationResult(true, null);
        }
    }
}
