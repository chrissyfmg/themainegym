﻿using System.Globalization;
using System.Windows.Controls;

namespace GymMembers.Domain.Validators
{
    public class NumericValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            long numberValue;
            if (!long.TryParse((string)value, out numberValue))
            {
                return new ValidationResult(false,
                  "You must enter a numeric value");
            }

            return new ValidationResult(true, null);
        }
    }
}
