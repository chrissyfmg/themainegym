﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace GymMembers.Domain.Validators
{
    public class MaxLengthValidator : ValidationRule
    {
        public int MaxLength { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var input = (string)value;
            if (input != null && input.Length > MaxLength)
            {
                return new ValidationResult(false,
                  String.Format("Length must be less than {0}", MaxLength));
            }

            return new ValidationResult(true, null);
        }
    }
}
