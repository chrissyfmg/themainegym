﻿using System.Globalization;
using System.Windows.Controls;

namespace GymMembers.Domain.Validators
{
    public class DecimalValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            decimal decimalValue;
            if (!decimal.TryParse((string)value, out decimalValue))
            {
                return new ValidationResult(false, null);
            }

            return new ValidationResult(true, null);
        }
    }
}
