﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;

namespace GymMembers.Domain.Validators
{
    public class NotNullOrEmptyValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value != null)
            {
                if (value is string)
                {
                    if (!string.IsNullOrWhiteSpace((string)value))
                    {
                        return new ValidationResult(true, null);
                    }
                }
                else
                {
                    return new ValidationResult(true, null);
                }
            }

            return new ValidationResult(false, null);
        }
    }
}
