﻿namespace GymMembers.Domain.Extensions
{
    public static class DecimalExtensions
    {
        public static decimal MinusPercentage(this decimal fullValue, int percentage)
        {
            var discountAmount = (fullValue/100)*percentage;

            return fullValue - discountAmount;
        }
    }
}
