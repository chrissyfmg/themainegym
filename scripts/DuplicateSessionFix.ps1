﻿[Reflection.Assembly]::LoadFile(“C:\Program Files\Microsoft SQL Server Compact Edition\v4.0\Desktop\System.Data.SqlServerCe.dll”)

$connectionString = "Data Source=" + $PSScriptRoot + "\PeakPhysique.sdf" 
$connection = new-object "System.Data.SqlServerCe.SqlCeConnection" $connectionString

$command = new-object "System.Data.SqlServerCe.SqlCeCommand" 
$command.Connection = $connection
$command.CommandType = [System.Data.CommandType]"Text" 
$command.CommandText = "SELECT s.Id SessionId, c.Id CustomerId, c.FirstName, c.LastName, s.StartTime
                    FROM Sessions s
                    INNER JOIN Customers c on c.ID = s.Customer_ID
                    WHERE EndTime IS NULL
                    AND s.Customer_ID IN (SELECT Customer_ID
                    					    FROM Sessions
                    					    WHERE EndTime IS NULL
                    					    GROUP BY Customer_ID
                    					    HAVING COUNT(*) > 1)" 

$connection.Open() 
$reader = $command.ExecuteReader()
$data = new-object "System.Data.DataTable"
$data.Load($reader) 
$data | Format-Table

$command.CommandText = "DELETE FROM Sessions
                               WHERE EndTime IS NULL
                                AND Customer_ID IN (SELECT Customer_ID
                    					            FROM Sessions
                    					            WHERE EndTime IS NULL
                    					            GROUP BY Customer_ID
                    					            HAVING COUNT(*) > 1)" 



$command.ExecuteNonQuery()

$connection.Close()

Pause